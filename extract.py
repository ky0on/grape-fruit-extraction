#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
--------------------------------------------
Extract fruit and index areas from images
by using OTSU thresholding method

@usage python extract.py PATH_TO_IMAGE_FOLDER
@author kyon <kyon.0308@gmail.com>
@created 2015/02/05
--------------------------------------------
"""

import sys
import cv2
import numpy as np
import os
import glob
import pandas as pd

if __name__=='__main__':

    #parameters
    ext = 'jpg'   # image extension
    tip = 0.1     # tip area ratio (will be removed)
    dir_fruit = 'fruit'   # save result image to this folder
    dir_index = 'index'   # save result image to this folder
    index_size = 4        # area of index [cm^2]
    csv = 'area.csv'

    #get image list in dir
    imgpaths = glob.glob(sys.argv[1]+'/*.'+ext)

    #save dir
    if not os.path.exists(dir_fruit): os.mkdir(dir_fruit)
    if not os.path.exists(dir_index): os.mkdir(dir_index)

    #store result to this array
    result = []

    #process images one by one
    for imgpath in imgpaths:
        
        #read image
        img = cv2.imread(imgpath)

        #remove tips of image
        h, w, d = img.shape
        margin_h, margin_w = tip * h, tip * w
        img = img[margin_h:h-margin_h, margin_w:w-margin_w]

        #thresholding
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        _, th = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)

        #extract large areas (fruit and index)
        contours, _ = cv2.findContours(th.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        large_contours = sorted(contours, key=cv2.contourArea, reverse=True)[:2]  # get the first and second largest areas
        fruit = np.zeros(th.shape)
        index = np.zeros(th.shape)
        cv2.drawContours(fruit, [large_contours[0]], -1, 255, thickness=-1)  # fruit area
        cv2.drawContours(index, [large_contours[1]], -1, 255, thickness=-1)  # index area

        #measure area
        fruit_area = len(np.where(fruit==255)[0])
        index_area = len(np.where(index==255)[0])
        ratio = round(fruit_area/float(index_area), 3)
        result.append([os.path.basename(imgpath), fruit_area, index_area, ratio, ratio*index_size])

        #save result images
        cv2.imwrite(dir_fruit+'/'+os.path.basename(imgpath), fruit)
        cv2.imwrite(dir_index+'/'+os.path.basename(imgpath), index)

    #save result as csv
    d = pd.DataFrame(result, columns=['filename', 'fruit area [px]', 'index area [px]', 'fruit area/index area', 'fruit area [cm^2]'])
    d.to_csv(csv, sep=',', index=False)
    print '# Result was saved as', csv
